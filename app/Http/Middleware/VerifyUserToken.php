<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;
use Illuminate\Http\Response;

class VerifyUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();

        if ($token) return $next($request);

        $response = array('message' => 'Sorry for the inconvenience, please login again');
        return new Response($response, 401);
    }
}
