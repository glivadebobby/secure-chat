<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMessageRequest;
use App\Message;
use App\Token;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class MessageController extends Controller
{
    public function message(SendMessageRequest $request)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();

        $message = Message::create(['sender_id' => $token->user_id,
            'receiver_id' => $request->receiver_id,
            'message' => $request->message]);

        $receiver = User::find($request->receiver_id);

        $message->sender = $token->user;

        $this->sendPushNotification($receiver->token->fcm_token, $message);

        return new Response($message, 200);
    }

    private function sendPushNotification($fcm_token, $message)
    {
        if ($fcm_token != null) {
            $client = new Client;
            $body['to'] = $fcm_token;
            $body['data'] = $message;
            $headers = ['Content-Type' => 'application/json', 'Authorization' => 'key=' . config('constant.FCM_KEY')];
            $client->post(config('constant.URL_FCM'), ['headers' => $headers, 'body' => json_encode($body)]);
        }
    }
}
