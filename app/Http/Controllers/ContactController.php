<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\UpdateContactRequest;
use App\Token;
use Illuminate\Http\Response;

class ContactController extends Controller
{
    public function update(UpdateContactRequest $request)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();

        $contacts = $request->input('contacts');
        foreach ($contacts as $contact) {
            Contact::updateOrCreate(['user_id' => $token->user_id, 'phone' => $contact['phone']], ['name' => $contact['name']]);
        }

        $response = Contact::with('user')->where('user_id', $token->user_id)->has('phone')->get();
        return new Response($response, 200);
    }
}
