<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendOtpRequest;
use App\Http\Requests\SignInRequest;
use App\Http\Requests\UpdateFcmTokenRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\VerifyPhoneRequest;
use App\Otp;
use App\Token;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function verifyPhone(VerifyPhoneRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();
        $otp = Otp::where([
            ['user_id', $user->id],
            ['otp_code', $request->otp_code]
        ])->first();

        if ($otp) {
            if ($this->isExpired($otp->expires_at)) {
                $response = array('message' => 'OTP Expired');
                return new Response($response, 400);
            }
            $otp->delete();

            $user->access_token = $this->generateToken($user->id);
            return new Response($user, 200);
        }

        $response = array('message' => 'Invalid OTP');
        return new Response($response, 400);
    }

    private function isExpired($expires_at)
    {
        return Carbon::now() > $expires_at;
    }

    private function generateToken($id)
    {
        $token = Token::updateOrCreate(['user_id' => $id], ['access_token' => Str::random(40) . sha1(time())]);
        return $token->access_token;
    }

    public function sendOtp(SendOtpRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        $response = $this->generateOtp($user);
        return new Response($response, 200);
    }

    private function generateOtp($user)
    {
        $otp_code = mt_rand(100000, 999999);
        $message = $otp_code . ' is your ' . config('app.name') . ' verification code. Code valid for 30 minutes only, one time use. Chat Securely!';

        $url = config('constant.URL_OTP') . '?authkey=' . config('constant.AUTH_KEY')
            . '&mobiles=' . $user->phone . '&message=' . urlencode($message)
            . '&sender=' . config('constant.SENDER_ID')
            . '&route=' . config('constant.ROUTE');
        $client = new Client;
        $client->get($url);

        $otp = new Otp;
        $otp->user_id = $user->id;
        $otp->otp_code = $otp_code;
        $otp->expires_at = Carbon::now()->addMinute(30);
        $otp->save();

        return array('message' => 'OTP code sent to ' . $user->phone);
    }

    public function signIn(SignInRequest $request)
    {
        $user = User::firstOrCreate(['phone' => $request->phone]);

        $response = $this->generateOtp($user);
        return new Response($response, 200);
    }

    public function profile(UpdateProfileRequest $request)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();

        $user = User::find($token->user_id);

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store(config('constant.UPLOAD_PROFILE'), 'public');
            $user->image = $path;
            $user->save();
        }

        $response = array('message' => 'Profile updated');
        return new Response($response, 200);
    }

    public function fcmToken(UpdateFcmTokenRequest $request)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();
        $token->fcm_token = $request->fcm_token;
        $token->save();

        $response = array('message' => 'Fcm token updated');
        return new Response($response, 200);
    }

    public function signOut(Request $request)
    {
        $token = Token::where('access_token', $request->header('Authorization'))->first();
        $token->delete();

        $response = array('message' => 'You\'ve been logged out');
        return new Response($response, 200);
    }
}
