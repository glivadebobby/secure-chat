<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $dates = [
        'expires_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $guarded = [

    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
