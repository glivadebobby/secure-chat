<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $guarded = [

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'phone', 'phone');
    }

    public function phone()
    {
        return $this->belongsTo(User::class, 'phone', 'phone');
    }
}
