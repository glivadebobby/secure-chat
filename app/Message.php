<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $hidden = [
        'sender_id',
        'receiver_id',
        'is_delivered',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $guarded = [

    ];

    protected $casts = [
        'is_delivered' => 'boolean'
    ];

    public function setMessageAttribute($message)
    {
        $this->attributes['message'] = encrypt($message);
    }

    public function getMessageAttribute($message)
    {
        return decrypt($message);
    }
}
