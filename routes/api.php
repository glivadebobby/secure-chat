<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/user'], function () {

    Route::post('sign-in', 'UserController@signIn');
    Route::post('send-otp', 'UserController@sendOtp');
    Route::post('verify-phone', 'UserController@verifyPhone');
    Route::post('profile', 'UserController@profile')->middleware('user_token');
    Route::post('fcm-token', 'UserController@fcmToken')->middleware('user_token');
    Route::delete('sign-out', 'UserController@signOut')->middleware('user_token');

});

Route::group(['prefix' => 'v1/contact'], function () {

    Route::post('/', 'ContactController@update')->middleware('user_token');

});

Route::group(['prefix' => 'v1/message'], function () {

    Route::post('/', 'MessageController@message')->middleware('user_token');

});

Route::group(['prefix' => 'v1/block'], function () {

    Route::post('/', 'BlockController@block')->middleware('user_token');

});
